require 'rails_helper'

RSpec.describe Item, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:item)).to be_valid
      expect(build(:item).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:name).of_type(:string) }
      it { is_expected.to have_db_column(:description).of_type(:string) }
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user_inventory).required(false) }
    it { is_expected.to have_many(:item_stats).dependent(:destroy) }
  end

  describe 'validations' do
    describe 'name' do
      it { is_expected.to validate_presence_of(:name) }
      it { is_expected.to validate_length_of(:name).is_at_most(24) }
    end

    describe 'description' do
      it do
        is_expected.to validate_length_of(:description)
          .is_at_least(20).is_at_most(200)
      end
    end
  end
end
