require 'rails_helper'

RSpec.describe Stat, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:stat)).to be_valid
      expect(build(:stat).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:id).of_type(:integer) }
      it { is_expected.to have_db_column(:name).of_type(:string) }
      it { is_expected.to have_db_column(:short_name).of_type(:string) }
      it { is_expected.to have_db_column(:description).of_type(:text) }
      it { is_expected.to have_db_column(:user_stat_id).of_type(:integer) }
      it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
      it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user_stat).required(false) }
  end

  describe 'validations' do
    describe 'name' do
      it { is_expected.to validate_presence_of(:name) }
    end

    describe 'short_name' do
      it { is_expected.to validate_presence_of(:short_name) }
      it { is_expected.to validate_uniqueness_of(:short_name) }
      it { is_expected.to validate_length_of(:short_name).is_at_most(16) }
    end
  end
end
