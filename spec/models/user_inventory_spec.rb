require 'rails_helper'

RSpec.describe UserInventory, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:user_inventory)).to be_valid
      expect(build(:user_inventory).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:quantity).of_type(:integer) }
    end
  end

  describe 'associations' do
      it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    describe 'quantity' do
      it { is_expected.to validate_numericality_of(:quantity).is_greater_than_or_equal_to(1) }
    end
  end
end
