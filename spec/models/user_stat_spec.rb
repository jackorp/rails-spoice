require 'rails_helper'

RSpec.describe UserStat, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:user_stat)).to be_valid
      expect(build(:user_stat).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:id).of_type(:integer) }
      it { is_expected.to have_db_column(:value).of_type(:string) }
      it { is_expected.to have_db_column(:user_id).of_type(:integer) }
      it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
      it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to have_one(:stat) }
    it { is_expected.to have_one(:proficiency) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:value) }
  end
end
