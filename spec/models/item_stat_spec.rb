require 'rails_helper'

RSpec.describe ItemStat, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:item_stat)).to be_valid
      expect(build(:item_stat).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:value).of_type(:string) }
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:item).required(false) }
    it { is_expected.to have_one(:stat) }
    it { is_expected.to have_one(:proficiency).dependent(:destroy) }
  end

  describe 'validations' do
    describe 'value' do
      it { is_expected.to validate_presence_of(:value) }
    end
  end
end
