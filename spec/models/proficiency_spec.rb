require 'rails_helper'

RSpec.describe Proficiency, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:proficiency)).to be_valid
      expect(build(:proficiency).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:id).of_type(:integer) }
      it { is_expected.to have_db_column(:level).of_type(:integer) }
      it { is_expected.to have_db_column(:experience).of_type(:integer) }
      it { is_expected.to have_db_column(:user_stat_id).of_type(:integer) }
      it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
      it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user_stat).required(false) }
    it { is_expected.to belong_to(:item_stat).required(false) }
  end

  describe 'validations' do
    describe 'level' do
      it { is_expected.to validate_numericality_of(:level).is_greater_than_or_equal_to(1) }
    end

    describe 'experience' do
      it { is_expected.to validate_numericality_of(:experience).is_greater_than_or_equal_to(0) }
    end

  end

  describe 'leveling up proficiency' do
    let(:proficiency) { build(:proficiency) }

    describe '#experience_to_next_level' do
      let(:experience_formula) { 100 + 10 * proficiency.level }

      subject { proficiency.experience_to_next_level }

      it { is_expected.to eq(experience_formula) }
    end

    describe '#gold_to_next_level' do
      let(:gold_formula) { 100 + 10 * proficiency.level }

      subject { proficiency.experience_to_next_level }

      it { is_expected.to eq(gold_formula) }
    end
  end
end
