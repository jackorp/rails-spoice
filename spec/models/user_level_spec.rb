require 'rails_helper'

RSpec.describe UserLevel, type: :model do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:user_level)).to be_valid
      expect(build(:user_level).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:level).of_type(:integer) }
      it { is_expected.to have_db_column(:experience).of_type(:integer) }
    end
  end

  describe 'associations' do
      it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    describe 'level' do
      it { is_expected.to validate_numericality_of(:level).is_greater_than_or_equal_to(1) }
      it { is_expected.to validate_presence_of(:level) }
    end

    describe 'experience' do
      it { is_expected.to validate_numericality_of(:experience).is_greater_than_or_equal_to(0) }
    end
  end
end
