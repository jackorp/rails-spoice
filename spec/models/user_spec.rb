require 'rails_helper'

RSpec.describe User, type: :model do
  ActiveJob::Base.queue_adapter = :test

  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:user)).to be_valid
      expect(build(:user).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
      it { is_expected.to have_db_column(:gold).of_type(:integer) }
      it { is_expected.to have_db_column(:away).of_type(:boolean) }
    end
  end

  describe 'associations' do
    it { is_expected.to have_many(:stats).class_name('UserStat').dependent(:destroy) }
    it { is_expected.to have_one(:level).class_name('UserLevel').dependent(:destroy) }
  end

  describe 'validations' do
    describe 'away' do
      let(:user) { build(:user, :away => true) }
      
      context 'when away is true' do
        it 'is expected to raise error on new_task' do
          user.valid?(:new_task)
          expect(user.errors.messages).to include({:away => ["cannot start multiple concurrent tasks."]})
        end
      end
    end
  end

  describe '#mine_asteroid_belt' do
    let(:user) { build(:user) }

    before do
      user.save
    end

    context 'mine for 5 minutes' do
      it 'sets away to true' do
        user.mine_asteroid_belt({ :minutes => 5 })
        expect(user.away).to be true
      end

      it 'creates AsteroidMineJob' do
        expect { AsteroidMineJob.perform_later(user, 1) }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
      end
    end

    context 'argument is not a hash' do
      it 'throws TypeError with custom message' do
        expect{ user.mine_asteroid_belt([1,2,3]) }.to raise_error TypeError, 'Duration must be a hash.'
        expect{ user.mine_asteroid_belt('foo') }.to raise_error TypeError, 'Duration must be a hash.'
      end
    end

    context 'hash is empty' do
      it 'throws ArgumentError with custom message' do
        expect{ user.mine_asteroid_belt() }.to raise_error ArgumentError, 'Duration cannot be empty.'
        expect{ user.mine_asteroid_belt({}) }.to raise_error ArgumentError, 'Duration cannot be empty.'
      end
    end
  end

  describe '#award_asteroid_mining' do
    let(:user) { build(:user) }

    context '5 minute mining' do
      it 'adds gold to the user based on duration_in_seconds' do
        expect { user.award_asteroid_mining(5*60) }
          .to change { user.gold }
          .by(5*60 / 5 + 1)
      end
    end
  end

  describe '#duration_in_seconds' do
    context 'returns seconds correctly' do
      let(:user) { build(:user) }

      it 'when only seconds are defined' do
        expect(user.duration_in_seconds({:seconds => 1})).to eq 1
      end

      it 'when only minutes are defined' do
        expect(user.duration_in_seconds({:minutes => 1})).to eq 60
      end

      it 'when only hours are defined' do
        expect(user.duration_in_seconds({:hours => 1})).to eq 3600
      end

      it 'when seconds, minutes and hours are defined' do
        expect(user.duration_in_seconds({ :seconds => 1, :minutes => 1, :hours => 1 })).to eq 3661
      end
    end
  end

  describe '#init' do
    subject { create(:user) }

    context 'initializes user_level' do
      its(:level) { is_expected.to be_an UserLevel }
    end

    context 'initializes stats from Stats::PLAYER_STATS' do
      its("stats.sample") { is_expected.to be_a UserStat }
      its("stats.size") { is_expected.to eq Stat::PLAYER_STATS.size }
    end
  end
end
