require 'rails_helper'

RSpec.describe AsteroidMineJob, type: :job do
  ActiveJob::Base.queue_adapter = :test
  include ActiveJob::TestHelper

  let(:user) {  build(:user, away: true) }
  subject(:job) { described_class.perform_later(user, 1) }

  before do
    user.save
  end


  it "queues the job" do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it "matches with enqueued job" do
    expect { described_class.perform_later }.to have_enqueued_job(described_class)
  end

  it "is in default queue" do
    expect(described_class.new.queue_name).to eq('task')
  end

  it "executes perform" do
    perform_enqueued_jobs { job }
    expect(User.first.away).to be false
  end
end
