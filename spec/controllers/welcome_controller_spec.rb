require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do

  describe "GET #index" do
    context 'user is signed in' do
      before { sign_in create(:user) }
      it "returns http success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    context 'user is not signed in' do
      it 'returns http redirect' do
        get :index
        expect(response).to have_http_status(:redirect)
      end
    end
  end
end
