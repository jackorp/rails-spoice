require 'rails_helper'

RSpec.describe AsteroidBeltMineController, type: :controller do
  before do
    sign_in create(:user)
  end

  describe 'POST #create' do
      before do
        post :create, :params => { :duration => { :hours => 0 } }
      end

      it 'returns http success' do
        expect(response).to have_http_status(:redirect)
      end
      
      it 'redirects to welcome#index' do
        expect(response).to redirect_to('/welcome/index')
      end
  end

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
end
