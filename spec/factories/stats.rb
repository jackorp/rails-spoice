FactoryBot.define do
  factory :stat do
    name { "MyString" }
    short_name { "MyString" }
    description { "MyText" }

    user_stat
  end
end
