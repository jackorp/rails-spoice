FactoryBot.define do
  factory :user_inventory do
    user
    quantity { 1 }
  end
end
