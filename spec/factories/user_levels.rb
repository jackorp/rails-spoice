FactoryBot.define do
  factory :user_level do
    level { 1 }
    experience { 1 }
    user
  end
end
