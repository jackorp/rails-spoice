FactoryBot.define do
  factory :proficiency do
    level { 1 }
    experience { 0 }
    user_stat
  end
end
