FactoryBot.define do
  factory :item_stat do
    item
    value { "MyString" }
  end
end
