FactoryBot.define do
  factory :item do
    user_inventory { nil }
    name { "MyString" }
    description { "MyStringThatIsLongAndDescriptive" }
  end
end
