require 'rails_helper'

<% module_namespacing do -%>
RSpec.describe <%= class_name %>, <%= type_metatag(:model) %> do
  context 'valid factory' do
    it 'saves the factory' do
      expect(build(:<%= class_name.downcase %>)).to be_valid
      expect(build(:<%= class_name.downcase %>).save).to be true
    end
  end

  describe 'database' do
    describe 'columns' do
    <%- attributes.each do |attribute| -%>
      <%- next if attribute.type == :references -%>
      it { is_expected.to have_db_column(:<%= attribute.name %>).of_type(:<%= attribute.type %>) }
    <%- end -%>
    end
  end

  describe 'associations' do
    <%- attributes.each do |attribute| -%>
      <%- next unless attribute.type == :references -%>
      it { is_expected.to belong_to(:<%= attribute.name %>) }
    <%- end -%>
  end

  describe 'validations' do
  <%- attributes.each do |attribute| -%>
    <%- next if attribute.type == :references -%>
    describe '<%= attribute.name %>' do
    end

  <%- end -%>
  end
end
<% end -%>
