Rails.application.routes.draw do
  devise_for :users

  authenticated :user do
    root to: 'welcome#index', as: :authenticated_root
  end

  patch 'asteroid_belt_mine/create'
  get 'asteroid_belt_mine/new'

  get 'welcome/index'

  get 'initial/index'
  root to: 'initial#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
