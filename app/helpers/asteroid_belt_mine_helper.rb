module AsteroidBeltMineHelper
  def self.duration_and_gold
    (1..10).map.with_index { |hour, number| "#{number + 1 } hours for #{(hour * 3600 / 43 + 1)} gold" }
  end
end
