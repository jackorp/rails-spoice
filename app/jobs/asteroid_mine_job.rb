class AsteroidMineJob < ApplicationJob
  queue_as :task

  # @param user [User] user object
  # @param duration [Integer] how long was this set to wait in seconds
  def perform(user, duration)
    user.update({ :away => false })
    user.award_asteroid_mining(duration)
  end
end
