class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  after_create :init

  validate :away_cannot_be_true_when_starting_new_task, on: :new_task

  has_many :stats, :class_name => 'UserStat', :dependent => :destroy
  has_one :level, :class_name =>  'UserLevel', :dependent => :destroy
  has_many :items, :class_name => 'UserInventory', :dependent => :destroy

  # @param duration [Hash] example: +{:seconds => 0, :minutes => 5, :hours => 0}+ lasts for 5 minutes
  # Any combination of time durations is fine e.g. {:minutes => 5} is valid as well as {:seconds => 15, :hours => 1}
  # keep the durations sane, if you go over 23 hours 59 minutes 59 seconds, expect resistance from the program
  def mine_asteroid_belt(duration = {})
    raise TypeError, 'Duration must be a hash.' unless duration.is_a? Hash
    raise ArgumentError, 'Duration cannot be empty.' if duration.empty?

    update({ :away => true })

    duration = duration_in_seconds duration
    waiting_duration = Time.now.advance(:seconds => duration)
    AsteroidMineJob.set(wait_until: waiting_duration).perform_later(self, duration)
  end

  # @param duration [Integer] Duration in *seconds only*, used to calculate reward, passed from the AsteroidMineJob
  def award_asteroid_mining(duration)
    self.away = false
    self.gold += duration / 5 + 1 # whatever formula, not like I care
    save
  end

  def duration_in_seconds(duration)
    seconds = duration[:seconds].to_i
    minutes = duration[:minutes].to_i
    hours = duration[:hours].to_i
    seconds + minutes * 60 + hours * 60 * 60
  end

  private

  # @return [true] if user has away set to false, otherwise raise an error on the model and return false
  def away_cannot_be_true_when_starting_new_task
    if self.away
      errors.add(:away, "cannot start multiple concurrent tasks.")
    end
  end

  def init
    create_level(:level => 1, :experience => 0)
    Stat::PLAYER_STATS.each do |stat_hash|
      stats.create(:value => 0).stat = Stat.find_by_name(stat_hash[:name])
    end
  end
end
