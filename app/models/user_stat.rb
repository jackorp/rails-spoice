class UserStat < ApplicationRecord
  belongs_to :user
  has_one :stat
  has_one :proficiency

  after_create :init

  validates :value, :presence => true

  private

  def init
    self.proficiency = Proficiency.create(:experience => 0)
  end
end
