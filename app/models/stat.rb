class Stat < ApplicationRecord
  belongs_to :user_stat, :optional => true
  belongs_to :item_stat, :optional => true

  PLAYER_STATS = [
    { :name => 'piloting', :short_name => 'pilot' },
    { :name => 'maneuvring', :short_name => 'manev' },
    { :name => 'mining', :short_name => 'mine' },
    { :name => 'engineering', :short_name => 'eng' },
    { :name => 'trading', :short_name => 'trade' }
  ].freeze

  GUN_STATS = [
    { :name => 'ammo_clip_capacity', :short_name => 'ammo_clp' },
    { :name => 'ammo_capacity', :short_name => 'ammo_cap' },
    { :name => 'rate_of_fire', :short_name => 'rof' },
    { :name => 'effective_range', :short_name => 'eff_rg' },
    { :name => 'modifier', :short_name => 'mod' }
  ].freeze

  ARMOR_STATS = [
    { :name => 'armor_rating', :short_name => 'rating' },
    { :name => 'armor_type', :short_name => 'type' }, # TODO: validation on valid type eg armor type can only be 'shield', 'heavy', 'light', ...
    { :name => 'modifier', :short_name => 'mod' }
  ].freeze

  validates :name,
    :presence => true

  validates :short_name,
    :presence => true,
    :length => { :maximum => 16 },
    :uniqueness => true
end
