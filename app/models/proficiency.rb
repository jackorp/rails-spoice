class Proficiency < ApplicationRecord
  belongs_to :user_stat, :optional => true
  belongs_to :item_stat, :optional => true

  validates :experience,
    :numericality => { :greater_than_or_equal_to => 0 }

  validates :level,
    :numericality => { :greater_than_or_equal_to => 1 }

  def experience_to_next_level
    100+10*self.level
  end

  def gold_to_next_level
    10+10*self.level
  end
end
