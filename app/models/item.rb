class Item < ApplicationRecord
  belongs_to :user_inventory, :optional => true
  has_many :item_stats, :dependent => :destroy

  validates :name,
    :presence => true,
    :length => { :maximum => 24 }
  validates :description,
    :length => { :in => 20..200 }
end
