class ItemStat < ApplicationRecord
  belongs_to :item, :required => false
  has_one :stat
  has_one :proficiency, :dependent => :destroy

  after_create :init

  validates :value,
    :presence => true

  private

  def init
    self.proficiency = Proficiency.create(:experience => 0)
  end
end
