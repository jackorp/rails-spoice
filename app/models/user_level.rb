class UserLevel < ApplicationRecord
  belongs_to :user

  validates :level,
    :presence => true,
    :numericality => { :greater_than_or_equal_to => 1 }

  validates :experience,
    :numericality => { :greater_than_or_equal_to => 0 }
end
