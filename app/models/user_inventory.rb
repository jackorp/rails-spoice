class UserInventory < ApplicationRecord
  belongs_to :user
  has_many :items

  validates :quantity,
    :numericality => { :greater_than_or_equal_to => 1 }
end
