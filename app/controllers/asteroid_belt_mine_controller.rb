class AsteroidBeltMineController < ApplicationController
  before_action :authenticate_user!

  def create
    if current_user.valid?(:new_task)
      current_user.mine_asteroid_belt({
          :hours => duration_params[:hours].to_i
        })
      current_user.update({:away => true})

      redirect_to welcome_index_url
    else
      render :new
    end
  end

  def new
  end

  private

  def duration_params
    params.require(:duration).permit(:hours)
  end
end
