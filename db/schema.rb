# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_19_140812) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "item_stats", force: :cascade do |t|
    t.bigint "item_id"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_item_stats_on_item_id"
  end

  create_table "items", force: :cascade do |t|
    t.bigint "user_inventory_id"
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_inventory_id"], name: "index_items_on_user_inventory_id"
  end

  create_table "proficiencies", force: :cascade do |t|
    t.integer "level", default: 1, null: false
    t.integer "experience"
    t.bigint "user_stat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "item_stat_id"
    t.index ["item_stat_id"], name: "index_proficiencies_on_item_stat_id"
    t.index ["user_stat_id"], name: "index_proficiencies_on_user_stat_id"
  end

  create_table "stats", force: :cascade do |t|
    t.string "name"
    t.string "short_name", limit: 16
    t.text "description", default: "To be done."
    t.bigint "user_stat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "item_stat_id"
    t.index ["item_stat_id"], name: "index_stats_on_item_stat_id"
    t.index ["user_stat_id"], name: "index_stats_on_user_stat_id"
  end

  create_table "user_inventories", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_inventories_on_user_id"
  end

  create_table "user_levels", force: :cascade do |t|
    t.integer "level"
    t.integer "experience"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_levels_on_user_id"
  end

  create_table "user_stats", force: :cascade do |t|
    t.string "value"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_stats_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "gold", default: 0, null: false
    t.boolean "away", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "item_stats", "items"
  add_foreign_key "items", "user_inventories"
  add_foreign_key "proficiencies", "item_stats"
  add_foreign_key "proficiencies", "user_stats"
  add_foreign_key "stats", "item_stats"
  add_foreign_key "stats", "user_stats"
  add_foreign_key "user_inventories", "users"
  add_foreign_key "user_levels", "users"
  add_foreign_key "user_stats", "users"
end
