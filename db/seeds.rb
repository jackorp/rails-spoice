Stat::PLAYER_STATS.each_with_index do |stat_hash|
  Stat.find_or_create_by!(stat_hash)
end

Stat::GUN_STATS.each_with_index do |stat_hash|
  Stat.find_or_create_by!(stat_hash)
end

Stat::ARMOR_STATS.each_with_index do |stat_hash|
  Stat.find_or_create_by!(stat_hash)
end

if Rails.env.development?
  User.create_with(:password => 'passpass', :password_confirmation => 'passpass')
    .find_or_create_by!(:email => 'jon@doe.com')
end
