class CreateStats < ActiveRecord::Migration[5.2]
  def change
    create_table :stats do |t|
      t.string :name
      t.string :short_name, limit: 16
      t.text :description, default: "To be done."
      t.references :user_stat, foreign_key: true

      t.timestamps
    end
  end
end
