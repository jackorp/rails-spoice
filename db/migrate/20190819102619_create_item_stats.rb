class CreateItemStats < ActiveRecord::Migration[5.2]
  def change
    create_table :item_stats do |t|
      t.references :item, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
