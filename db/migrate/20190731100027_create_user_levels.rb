class CreateUserLevels < ActiveRecord::Migration[5.2]
  def change
    create_table :user_levels do |t|
      t.integer :level
      t.integer :experience
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
