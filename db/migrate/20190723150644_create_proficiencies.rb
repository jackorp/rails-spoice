class CreateProficiencies < ActiveRecord::Migration[5.2]
  def change
    create_table :proficiencies do |t|
      t.integer :level, default: 1, null: false
      t.integer :experience
      t.references :user_stat, foreign_key: true

      t.timestamps
    end
  end
end
