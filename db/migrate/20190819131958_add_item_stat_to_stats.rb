class AddItemStatToStats < ActiveRecord::Migration[5.2]
  def change
    add_reference :stats, :item_stat, foreign_key: true
  end
end
