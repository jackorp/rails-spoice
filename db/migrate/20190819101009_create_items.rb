class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.references :user_inventory, foreign_key: true
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
