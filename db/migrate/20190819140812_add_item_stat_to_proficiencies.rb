class AddItemStatToProficiencies < ActiveRecord::Migration[5.2]
  def change
    add_reference :proficiencies, :item_stat, foreign_key: true
  end
end
